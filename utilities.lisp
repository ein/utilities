


(defpackage #:ein.utilities
  (:use :cl)
  (:local-nicknames (#:a #:alexandria-2))
  (:export #:bind
           #:dorec
           #:repeat
           #:table
           #:equal-table
           #:dotable
           #:>>
           #:op
           #:splat
           #:splat-expand
           #:derive-splat
           #:defrecord
           #:quicksort
           #+sbcl #:zero-alien
           #:partition-if))


(in-package #:ein.utilities)



#||
The strategy here is relatively simple, but not quiet as simple as it could be.
The easiest strategy would be to recurse through the body and build the resulting
tree in the natural way with backquotes, but that runs into the risk of long BIND bodies
overflowing the stack, which is undesirable.
So instead the stack is modelled explicitly. The body is stepped through, with normal
forms being pushed on the (not control) stack, and forms which require nesting wrapped
in a closure to defer the construction of the tree to later.a
Afterwards, the stack is looped through and the above closures are applied to their
respective body chunks.
||#

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun make-defer (form)
    (lambda (body)
      `((unwind-protect (progn ,@body)
          ,form))))


  (defun make-mvb (vars form)
    (lambda (body)
      `((multiple-value-bind ,vars ,form
          ,@(let ((locals (remove-if-not (lambda (v) (char= #\& (char (string v) 0))) vars)))
              (when locals
                `((declare (dynamic-extent ,@locals)))))
          ,@body))))


  (defun make-let (var form)
    (check-type var symbol)
    (lambda (body)
      `((let ((,var ,form))
          ,@(when (char= #\& (char (string var) 0))
              `((declare (dynamic-extent ,var))))
          ,@body))))


  (defun process-body (body)
    (let ((result nil)
          (stack nil)
          (body body))
      (loop
        (when (null body) (return))
        (destructuring-bind (a &optional b c &rest rest) body
          (cond ((eq a :defer)
                 (push (make-defer b) stack)
                 (setf body (cons c rest)))
                ((eq b :=)
                 (if (consp a)
                     (push (make-mvb a c) stack)
                     (push (make-let a c) stack))
                 (setf body rest))
                (t
                 (push a stack)
                 (pop body)))))
      (dolist (elem stack)
        (if (functionp elem)
            (setf result (funcall elem result))
            (push elem result)))
      result)))


(defmacro bind (&body body)
  "General purpose binding and sequence macro.

body         ::= binding-spec*
binding-spec ::= form | var := form [declaration]* | (var*) := form [declaration]* | :defer form

form        - a lisp form
var         - a symbol, not evaluated
:=          - the literal keyword-symbol ':='
:defer      - the literal keyword-symbol ':DEFER'
declaration - a declaration expression

BIND parses BODY, looking at up to 3 forms at a time. If the middle of the 3 considered forms is ':=',
it is considered a binding spec and expanded into a LET if VAR is a SYMBOL, or into a MULTIPLE-VALUE-BIND
if VAR is a list of symbols.
If the first of the 3 considered forms is ':DEFER', then the following form will be used as the cleanup form
while the rest of BODY is wrapped as a PROGN to be the primary form for an UNWIND-PROTECT.
Otherwise, the first form is used as-is, and parsing restarts with the second of the three forms.
Additionally, if any VAR starts with the character '&' it will be declared dynamic-extent, with all that implies.
The resulting PROGN is then wrapped in a BLOCK with tag NIL.
It is an ERROR for any VAR to be a KEYWORD."
  (list* 'block nil (process-body body)))




(defmacro dorec ((&rest bindings) &body body)
  "Looping construct emulating a common form of recursion.

bindings ::= binding*
binding  ::= (var form)
body     ::= [declaration]* form*

var         - a symbol, not evaluated
form        - a lisp form
declaration - a declaration expression

DOREC binds every VAR to its respective FORM, and executes BODY.
During the lexical extent of BODY, a form of the shape (REPEAT . ARGS) is bound to cause
control to return to the top of BODY. ARGS must be of the same length as BINDINGS, and each
value in ARGS will be bound to the corresponding VAR of a BINDING by order of arguments.

It is an error to attempt to use REPEAT outside of the lexical extent of BODY.

The intent is for DOREC to expand into what is semantically equivalent to
   (labels ((repeat (params...) ,@body))
     (repeat ,@args))
but avoid stack overflow on implementations which don't support TRO or TCO.

Examples:

(dorec ((a 0) (b 1) (n 50))
  (if (zerop n)
      a
      (repeat (+ a b) a (1- n))))"
  #+(or sbcl ccl cmucl allegro lispworks)
  (bind
    (params inits) := (loop :for (name init) :in bindings
                            :collect name :into names
                            :collect init :into inits
                            :finally (return (values names inits)))
    `(labels ((repeat ,params (block nil (locally ,@body))))
       (declare (dynamic-extent #'repeat))
       (repeat ,@inits)))
    
  #-(or sbcl ccl cmucl allegro lispworks)
  (bind
    repeat := (gensym "REPEAT")
    params := (loop :for (name _) :in bindings :collect name)
    `(macrolet ((repeat (&rest args)
                  (let ((setters (loop :for n :in ',params :for a :in args :append `(,n ,a))))
                    `(progn
                       (psetf ,@setters)
                       (go ,',repeat)))))
       (prog ,bindings
          ,repeat
          (return (locally ,@body))))))




(defun table (&rest pairs)
  "HASH-TABLE construction function.

pairs - alternating forms of keys and values

Returns a fresh EQL-specialied HASH-TABLE containing the key-value pairs in PAIRS.
If (LENGTH PAIRS) is odd, the final object is considered a key and stored in the table with value NIL.

Example:
(defvar *table* (table :a 1 :b 2)) => *TABLE*
(gethash :a *table*) => 1"
  (declare (dynamic-extent pairs))
  (let ((ht (make-hash-table)))
    (loop :for (key val) :on pairs :by #'cddr :do
      (setf (gethash key ht) val))
    ht))


(defun equal-table (&rest pairs)
  "HASH-TABLE construction function.

pairs - alternating forms of keys and values

Like TABLE, but constructs an EQUAL-specialised HASH-TABLE instead."
  (declare (dynamic-extent pairs))
  (let ((ht (make-hash-table :test #'equal)))
    (loop :for (key val) :on pairs :by #'cddr :do
      (setf (gethash key ht) val))
    ht))


(define-compiler-macro table (&rest pairs)
  (a:with-gensyms (ht)
    `(let ((,ht (make-hash-table)))
       (setf ,@(loop :for (key val) :on pairs :by #'cddr :append `((gethash ,key ,ht) ,val)))
       ,ht)))


(define-compiler-macro equal-table (&rest pairs)
  (a:with-gensyms (ht)
    `(let ((,ht (make-hash-table :test 'equal)))
       (setf ,@(loop :for (key val) :on pairs :by #'cddr :append `((gethash ,key ,ht) ,val)))
       ,ht)))


(defmacro dotable ((key-symbol val-symbol table &optional return-val) &body body)
  `(progn
     (maphash (lambda (,key-symbol ,val-symbol) ,@body) ,table)
     ,return-val))





;;; heap


(deftype predicate () '(function (t t) t))


(defstruct (heap
            :copier
            :predicate
            (:constructor %make-heap))
  (buffer (make-array 64) :type (simple-array t (*)))
  (fp 0 :type fixnum)
  (test (error "TEST must be supplied") :type predicate))





(declaim (inline heap-size heap-empty-p heap-peek make-heap))
(defun make-heap (test)
  (check-type test function)
  (%make-heap :test test))


(defun heap-size (heap)
  (heap-fp heap))

(defun heap-empty-p (heap)
  (zerop (heap-size heap)))


(defun heap-peek (heap)
  (if (heap-empty-p heap)
      (values nil nil)
      (values (aref (heap-buffer heap) 0) t)))


(defun sift-up (buffer idx test)
  (declare (optimize speed (debug 0))
           (type (simple-array t (*)) buffer)
           (type predicate test)
           (type fixnum idx))
  (loop :with i fixnum := idx :until (zerop i) :do
    (let* ((this (aref buffer i))
           (pidx (floor (1- i) 2))
           (parent (aref buffer pidx)))
      (unless (funcall test this parent)
        (return))
      (setf (aref buffer i) parent)
      (setf (aref buffer pidx) this)
      (setf i pidx))))



(defun sift-down (buffer fp idx test)
  (declare (optimize speed (debug 0))
           (type (simple-array t (*)) buffer)
           (type predicate test)
           (type fixnum idx fp))
  (locally (declare (optimize (safety 0)))
    (loop :with i fixnum := idx :until (<= fp (1+ (* i 2))) :do
      (let* ((this (aref buffer i))
             (lefti  (+ 1 (* 2 i)))
             (righti (+ 2 (* 2 i)))
             (left (aref buffer lefti))
             (right (aref buffer righti)))
        (multiple-value-bind (smaller smalleri)
            (if (funcall test left right)
                (values left lefti)
                (values right righti))
          (unless (funcall test smaller this)
            (return))
          (setf (aref buffer smalleri) this)
          (setf (aref buffer i) smaller)
          (setf i smalleri))))))


(defun heap-pop (heap)
  (declare (optimize speed)
           (type heap heap))
  (if (heap-empty-p heap)
      (values nil nil)
      (let* ((buffer (heap-buffer heap))
             (fp (heap-fp heap))
             (item (aref buffer 0)))
        (setf (aref buffer 0) (aref buffer (1- fp)))
        (decf (heap-fp heap))
        (sift-down buffer (1- fp) 0 (heap-test heap))
        (values item t))))


(defun heap-push (obj heap)
  (declare (optimize speed)
           (type heap heap))
  (let ((buffer (heap-buffer heap))
        (fp (heap-fp heap)))
    ;; ensure heap is big enough
    (when (>= fp (length buffer))
      (let ((new (make-array (* 2 (length buffer)))))
        (replace new buffer)
        (setf buffer new
              (heap-buffer heap) new)))
    (setf (aref buffer fp) obj)
    (incf (heap-fp heap))
    (sift-up buffer fp (heap-test heap))
    obj))



#+nil(defun test ()
       (declare (optimize speed (safety 0)))
       (let ((heap (make-heap (lambda (a b) (declare (type fixnum a b)) (< a b))))
             (iters 10000000))
         (time
          (loop :repeat iters :for i := (- (random 10000) 5000) :do (heap-push i heap)))
         #+nil(time (loop :repeat iters :for i := (- (random 10000) 5000) :do (heap-push i heap)))
         (time
           (loop :until (heap-empty-p heap) :do (heap-pop heap)))))




;; Partitions

(defun partition-if-list (predicate list)
  (let ((rest list)
        (stub nil))
    (loop :for item := (car rest) :do
      (if (funcall predicate item)
          (return (values (nreverse stub) rest))
          (push (pop rest) stub)))))



(defun partition-if-vec (predicate vector)
  (let ((pos (position-if-not predicate vector)))
    (values (subseq vector 0 pos) (subseq vector pos))))



(defun partition-if (predicate sequence)
  "Function to partition sequences according to a predicate

predicate - a function of one argument returning a generalized boolean
sequence - a sequence

PARTITION-IF invokes PREDICATE on every item of SEQUENCE until PREDICATE returns NIL.
It then returns 2 values: All items before the last tested items, and all remaining items."
  (etypecase sequence
    (list (partition-if-list predicate sequence))
    (vector (partition-if-vec predicate sequence))))




(defmacro >> (val &rest fns)
  "Macro to chain a series of accessors.

fns ::= fun

val - a form
fun - a symbol bound to a function"
  (reduce (lambda (v f) (list f v)) fns :initial-value val))




#||
Unlike serapeums version of OP, this one is much smaller in scope both in terms of
implementation and use-case, and i find it easier to reason about the expansion.
||#

(defmacro op (&body body)
  "Shorthand LAMBDA definition macro.

body ::= form*

form - a lisp form

OP constructs an equivalent LAMBDA expression by walking BODY as if by SUBST-IF, collecting
all symbols starting with '_' and treating them as lexographically sorted parameters to a lambda.
That is: 
  (OP + _1 2 (/ _C _B))
is equivalent to
  (lambda (|_1| B C) (+ _1 2 (/ _C _B)))

Note:
That BODY is parsed as if by SUBST-IF means that this implementation is not a robust code-walker as such,
and will for example mindlessly transform
  (op let ((_1 1)) (+ _1 _2))
into
  (lambda (_1 _2) (let ((_1 1)) (+ _1 _2)))
and thus require 2, not 1, arguments, as well as throw an unused parameter warning.
This could be a problem for long snippets using OP or macros expanding into OP, so
it's probably a bad idea to do."
  (bind
    params := nil
    (subst-if nil (lambda (v) (and (symbolp v) (char= #\_ (char (string v) 0)) (push v params)) nil) body)
    params := (sort (remove-duplicates params) #'string< :key #'string)
    `(lambda ,params ,body)))



(eval-when (:compile-toplevel :load-toplevel :execute)
  (defgeneric splat (object)
    (:documentation "Object destructuring generic.

object - an object

SPLAT returns the conceptual components of OBJECT as VALUES.
'Conceptual Components' should be the exposed fields of a datastructure or similar,
like for example the CAR/CDR of a list or the exported slots of a CLASS."))


  (defgeneric splat-expand (class-designator object)
    (:documentation "Object destructuring generic for type driven static expansions.

class-designator - a class-object, or a symbol naming a class
object           - an uninterned symbol representing the runtime object

The generic function SPLAT-EXPAND should return an expansion semantically equivalent to SPLAT
suitable for use in a compiler-macro. The expansion will be used, when possible, to eliminate the runtime dispatch of SPLAT
through a compiler macro.")))



(define-compiler-macro splat (&whole whole &environment env object)
  (declare (optimize debug))
  (bind
    obj := (gensym "OBJECT")
    class := (cond ((constantp object env) (class-of (eval object)))
                   ((symbolp object) (bind
                                       info := (nth-value 2 (cltl2:variable-information object env))
                                       type? := (cdr (assoc 'type info))
                                       (if (consp type?)
                                           (car type?)
                                           type?)))
                   (t nil))
    (when (not class)
      (return whole))
    expansion := (funcall #'splat-expand class obj)
    (if expansion
        `(let ((,obj ,object)) ,expansion)
        whole)))




(defmacro derive-splat (&environment env class-name &rest readers)
  "Convinience macro to automatically generate SPLAT and SPLAT-EXPAND definitions.

class-name - a symbol, not evaluated
readers - a list of symbols, not evaluated

DERIVE-SPLAT expands into definitions for SPLAT and SPLAT-EXPAND with the given CLASS-NAME and, if not NIL,
READERS.
When READERS is NIL, DERIVE-SPLAT will use MOP machinery to return as VALUEs all slots of the given CLASS.
In the face of multiple-inheritence, the order in which slots are returned is not defined.
Otherwise, READERS should name a list of readers which will be used to access an instance of CLASS
in the order provided.

Note:
It is implementation defined if structure-classes work or not. For compatibility reasons it is
strongly recommended to supply all readers of the structure definition you wish to be available
through SPLAT."
  (bind obj := (gensym (symbol-name class-name))
    (if readers
        `(progn
           (defmethod splat ((,obj ,class-name))
             (values ,@(loop :for reader :in readers :collect `(,reader ,obj))))
           (defmethod splat-expand ((c (eql ',class-name)) ,obj)
             `(values ,,@(loop :for reader :in readers :collect `(list ',reader ,obj)))))
        (bind
          class := (find-class class-name t env)
          (c2mop:finalize-inheritance class)
          slots := (c2mop:class-slots class)
          slot-names := (mapcar #'c2mop:slot-definition-name slots)
          `(progn
             (defmethod splat ((,obj ,class-name))
               (values ,@(loop :for name :in slot-names :collect `(slot-value ,obj ',name))))
             (defmethod splat-expand ((c (eql ',class-name)) ,obj)
               `(values ,,@(loop :for name :in slot-names :collect `(list 'slot-value  ,obj '',name)))))))))


(derive-splat cons car cdr)
(derive-splat ratio numerator denominator)
(derive-splat complex realpart imagpart)





;;; DEFRECORD

(defun optionsp (options-and-slots)
  (let ((opts (car options-and-slots)))
    (when (and (consp opts) (eq :options (car opts)))
      (cdr opts))))


(defun process-slot-for-struct (slot)
  (typecase slot
    (symbol (return-from process-slot-for-struct (list slot nil)))
    ((not cons) (error "Invalid slot-spec: ~a" slot)))
  ;; If we get here, it must be a cons
  (destructuring-bind (name &optional init type &rest flags) slot
    (let ((read-only? (find :read-only flags))
          (required? (find :required flags)))
      `(name
        ,(if required?
             `(error ,(format nil "Slot ~a is required." name))
             init)
        ,@(if type
              (list :type type)
              nil)
        ,@(if read-only?
              (list :read-only t)
              nil)))))


(defun process-slot-for-class (class-name)
  (lambda (slot)
    (block nil
      (typecase slot
        (symbol (return (list slot :initform nil :accessor (intern (format nil "~a-~a" class-name slot)))))
        ((not cons) (error "Invalid slot-spec: ~a" slot)))
      ;; If we get here, it must be a cons
      (destructuring-bind (name &optional init type &rest flags) slot
        (let ((read-only? (find :read-only flags))
              (required? (find :required flags)))
          `(,name
            ,@(if required?
                  `(:initform (error ,(format nil "Slot ~a is required." name)))
                  `(:initform ,init))
            ,@(if type
                  (list :type type)
                  nil)
            ,@(let ((acc-name (intern (format nil "~a-~a" class-name name))))
                (if read-only?
                    (list :reader acc-name)
                    (list :accessor acc-name)))))))))



(defun check-options-valid (options)
  (let ((valid-opts #(:seal :constructor :inherit)))
    (loop :for e :in options :by #'cddr :do
      (unless (find e valid-opts :test #'eq)
        (warn "~a is an unrecognised option to DEFRECORD" e)))))



(defmacro defrecord (name &body options-and-slots)
  (let* ((options (optionsp options-and-slots))
         (slots (if options (cdr options-and-slots) options-and-slots))
         sealed?
         constructor?
         inherit?)
    (check-options-valid options)
    (setf sealed? (getf options :seal))
    (setf constructor? (getf options :constructor))
    (setf inherit? (getf options :inherit))
    (if sealed?
        (setf slots (mapcar #'process-slot-for-struct slots))
        (setf slots (mapcar (process-slot-for-class name) slots)))
    (if sealed?
        `(defstruct
             ,(if (or constructor? inherit?)
                  name
                  `(,name ,@(when constructor? `((:constructor ,constructor?)))
                          ,@(when inherit? `((:include ,inherit?)))))
           ,@slots)
        
        `(progn
           (defclass ,name (,@(when inherit? `(,inherit?))) ,slots)
           (defun ,(if constructor?
                       constructor?
                       (intern (format nil "MAKE-~a" name)))
               (&key ,@(mapcar #'car slots))
             (let ((obj (make-instance ',name)))
               ,@(mapcar (lambda (slot) `(when ,slot (setf (slot-value obj ',slot) ,slot)))  (mapcar #'car slots))
               obj))))))





;;; Sorts

(declaim (inline choose-pivot))
(defun choose-pivot (vec pred lo hi)
  (let* ((mid (floor (+ lo hi) 2))
         (a (aref vec lo))
         (b (aref vec mid))
         (c (aref vec hi)))
    (cond ((funcall pred b a)
           (rotatef a b))
          ((funcall pred c a)
           (rotatef a c))
          ((funcall pred b c)
           (rotatef b c)))
    c))



(defmacro define-specialised-sorts (name array-type)
  (let ((quicksort-name (intern (format nil "%QUICKSORT-~a" name)))
        (selsort-name (intern (format nil "%SELECTION-SORT-~a" name))))
    
    `(locally (declare (optimize speed (safety 0) (debug 0))
                       #+sbcl(sb-ext:muffle-conditions sb-ext:compiler-note))

       (defun ,selsort-name (vec pred lo hi)
         (declare (type ,array-type vec)
                  (type function pred)
                  (type fixnum lo hi))
         (loop :for i :from lo :upto hi :do
           (let ((selection (aref vec i)))
             (loop :for j :from (1+ i) :upto hi :do
               (when (funcall pred (aref vec j) selection)
                 (rotatef selection (aref vec j))))
             (setf (aref vec i) selection)))
         vec)

       (defun ,quicksort-name (vec pred lo hi)
         (declare (type ,array-type vec)
                  (type function pred)
                  (fixnum lo hi))
         (when (<= hi lo) (return-from ,quicksort-name))
         (when (>= 10 (- hi lo)) (return-from ,quicksort-name (,selsort-name vec pred lo hi)))
         (let* ((pivot (choose-pivot vec pred lo hi)))
           (let ((i lo)
                 (j hi))
             (declare (type fixnum i j))
             (loop
               (loop :while (and (<= i hi) (funcall pred (aref vec i) pivot)) :do (incf i))
               (loop :while (funcall pred pivot (aref vec j)) :do (decf j))
               (when (> i j) (return))
               (rotatef (aref vec i) (aref vec j))
               (incf i)
               (decf j))
             (cond ((<= (- j lo) (- hi j))
                    (,quicksort-name vec pred lo j)
                    (,quicksort-name vec pred (1+ j) hi))
                   (t
                    (,quicksort-name vec pred (1+ j) hi)
                    (,quicksort-name vec pred lo j)))))))))


(define-specialised-sorts vector vector)
(define-specialised-sorts simple (simple-array t (*)))
(define-specialised-sorts u8 (simple-array (unsigned-byte 8) (*)))
(define-specialised-sorts u16 (simple-array (unsigned-byte 16) (*)))
(define-specialised-sorts u32 (simple-array (unsigned-byte 32) (*)))
(define-specialised-sorts s8 (simple-array (signed-byte 8) (*)))
(define-specialised-sorts s16 (simple-array (signed-byte 16) (*)))
(define-specialised-sorts s32 (simple-array (signed-byte 32) (*)))
(define-specialised-sorts fix (simple-array fixnum (*)))
(define-specialised-sorts f32 (simple-array single-float (*)))
(define-specialised-sorts f64 (simple-array double-float (*)))




;; Another angle of optimisation here would be some sort of "with-locally-optimised-sort"
;; macro that generates a sort specialised on both the array type and inlines the
;; predicate, as the calls to the predicate take up by far the most time in this current
;; version. 
(defun quicksort (vector predicate &key (start 0) (end (1- (length vector))))
  "Function implementing the quicksort-algorithm

vector - a vector
predicate - a function of two arguments, returning a generalised boolean
start - a fixnum >= 0
end - a fixnum < (length vector)

Destructively sorts the elements in VECTOR between START and END using the quicksort algorithm."
  (declare (optimize speed (debug 1) (safety 1)))
  (check-type end (integer 0 #.most-positive-fixnum))
  (unless (plusp end)
    (return-from quicksort vector))
  (check-type start (integer 0 #.most-positive-fixnum))
  (check-type vector vector)
  (check-type predicate function)
  (let ((vec #+sbcl(sb-ext:array-storage-vector vector) #-sbcl vector))
    (typecase vec
      ((simple-array t (*)) (%quicksort-simple vec predicate start end))
      ((simple-array (unsigned-byte 8) (*)) (%quicksort-u8 vec predicate start end))
      ((simple-array (unsigned-byte 16) (*)) (%quicksort-u16 vec predicate start end))
      ((simple-array (unsigned-byte 32) (*)) (%quicksort-u32 vec predicate start end))
      ((simple-array (signed-byte 8) (*)) (%quicksort-s8 vec predicate start end))
      ((simple-array (signed-byte 16) (*)) (%quicksort-s16 vec predicate start end))
      ((simple-array (signed-byte 32) (*)) (%quicksort-s32 vec predicate start end))
      ((simple-array fixnum (*)) (%quicksort-fix vec predicate start end))
      ((simple-array single-float (*)) (%quicksort-f32 vec predicate start end))
      ((simple-array double-float (*)) (%quicksort-f64 vec predicate start end))
      (t (%quicksort-vector vec predicate start end)))
    vector))



;;; Alien stuff
#+sbcl
(progn

  (defun zero-alien (sap size)
    "Function to zero an alien

sap - a sap
size - a fixnum

Sets all memory at the location pointed to by SAP for SIZE octets to 0.
See: SB-ALIEN:ALIEN-SAP, SB-ALIEN:ALIEN-SIZE"
    (dotimes (i size)
      (setf (sb-sys:sap-ref-8 sap i) 0)))

  (define-compiler-macro zero-alien (&environment env sap size)
    (let ((size (cond ((integerp size) size)
                      ((constantp size env) (eval size))
                      ;; presumably calling this alongside ALIEN-SIZE will be very common, so special case it
                      ((and (consp size) (eq (car size) 'sb-alien:alien-size)) (macroexpand size env)))))
      (if (not (integerp size))
        (a:with-gensyms (s)
          `(let ((,s ,sap))
             (dotimes (i ,size)
               (setf (sb-sys:sap-ref-8 ,s i) 0))))
        (multiple-value-bind (times-by-8 times-by-1) (floor size 8)
          (a:with-gensyms (s)
            `(let ((,s ,sap))
               (loop :repeat ,times-by-8 :for i :of-type fixnum := 0 :then (+ i 8) :do
                 (setf (sb-sys:sap-ref-64 ,s i) 0))
               (loop :repeat ,times-by-1 :for i :of-type fixnum := ,(* times-by-8 8) :then (1+ i) :do
                 (setf (sb-sys:sap-ref-8 ,s i) 0)))))))))








