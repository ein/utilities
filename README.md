

# ein.utilities
### yes, i too have my own utility library

Not all of these utilities may be palatable to hardcore lispers, but they make my life less miserable so i made them.  
All exported functions should be adequatly documented via standard doc strings. For convinience, here's a short index
of the functions and macros with short descriptions. 

#### BIND
BIND is a flat replacement for LET* and MULTIPLE-VALUE-BIND, with some more convinience functionality thrown in.

#### DOREC
DOREC is a looping-construct that emulates tail recursion while avoiding the pitfalls of CLs standard not requiring said optimization.  

REPEAT is an anaphoric pseudo-function which triggers a "recursion" during the dynamic-extent of DOREC.

#### TABLE and EQUAL-TABLE
TABLE and EQUAL-TABLE are short-hand functions to construct hashtables (of the EQL and EQUAL kind, respectively) and fill it
with key-value pairs.

#### DOTABLE
Like DOLIST, but for HASH-TABLEs.

#### OP
Stolen from GOO and/or serapeum, but with a much more simplified implementation and behaviour.
Makes writing short lambdas much more convinient.

#### SPLAT, SPLAT-EXPAND and DERIVE-SPLAT
SPLAT is a generic function to allow destructuring objects into multiple-values, which can be conviniently bound to by
BIND (or, if you want, MULTIPLE-VALUE-BIND) without requiring you to fall back to individual binding-macros like XYZ-BIND.

SPLAT-EXPAND, meanwhile, is another generic function designed to be called from a compiler-macro for SPLAT to avoid
the overhead of a funcall and method-dispatch that a generic function would usually bring.  

DERIVE-SPLAT can conviniently generate SPLAT and SPLAT-EXPAND implementations for objects given their CLASS-NAME
and a list of READERS to be exposed, to cut down on the boilerplate of writing both SPLAT and SPLAT-EXPAND.

Furthermore, EIN.UTILITIES pre-defines methods for CONSes to be destructured into their CAR and CDR, as well as RATIOs and COMPLEXEs
splatting into their respective components.


### Installation
Simply clone the repository to a place ASDF can find. Alternatively, all the functionality is self-contained in
utilities.lisp, including the package definition, so you can easily vendor it into your own project by just copying that one
file.
