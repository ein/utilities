
(asdf:defsystem #:ein.utilities
  :description "Some small utilities to make working in CL more comfy"
  :author "Ein Kathage <siboru@googlemail.com>"
  :license  "MIT"
  :version "1.0.0"
  :depends-on (:alexandria :trivial-cltl2 :closer-mop)
  :serial t
  :components ((:file "utilities")))
